package com.FUCKOFF.demo.model;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Component
public class JDBCResourcesDAO implements ResourceDAO {

    private JdbcTemplate jdbcTemplate;


    public JDBCResourcesDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Resource> motoQuote() {

        String sqlSearch = "SELECT body, details "+
                "FROM resources "+
                "WHERE category = 'quote' "+
                "ORDER BY random() LIMIT 1 ";

        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearch);
        List<Resource> motoQuotes = new ArrayList<>();

       while(results.next()){
         Resource resource = new Resource();
           resource.setBody(results.getString("body"));
           resource.setDetails(results.getString("details"));
           motoQuotes.add(resource);
       }
        return motoQuotes;
    }


    @Override
    public List<Resource> keywordSearch(String key) {

        String sqlSearch = "SELECT title, url, description "+
                "FROM student_resources "+
                "WHERE keyword LIKE :pname ";

        String finalName= "%" + key.toLowerCase().trim() + "%";
        MapSqlParameterSource namedParams= new MapSqlParameterSource();
        namedParams.addValue("pname", finalName);

        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearch);
        List<Resource> keywords = new ArrayList<>();

        while(results.next()){
            Resource resource = new Resource();
            resource.setTitle(results.getString("title"));
            resource.setUrl(results.getString("url"));
            resource.setDescription(results.getString("description"));
            keywords.add(resource);
        }
        return keywords;
    }



}

